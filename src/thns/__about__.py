# SPDX-FileCopyrightText: 2023-present Guido Kroon <guido@kroon.email>
#
# SPDX-License-Identifier: BSD-3-Clause
__version__ = "0.0.5"
